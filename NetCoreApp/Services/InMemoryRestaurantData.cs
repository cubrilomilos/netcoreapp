﻿using NetCoreApp.Entities;
using System.Collections.Generic;
using System.Linq;

namespace NetCoreApp.Services
{
    public class InMemoryRestaurantData : IRestaurantData
    {

        static InMemoryRestaurantData() //set this back to public later see 04-06
        {
            _restaurants = new List<Restaurant>
            {
                new Restaurant {Id=1, Name="restaurant 1"},
                new Restaurant {Id=2, Name="restaurant 2"},
                new Restaurant {Id=3, Name="restaurant 3"},
                new Restaurant {Id=4, Name="restaurant 4"}
            };
        }

        public IEnumerable<Restaurant> GetAll()
        {
            return _restaurants;
        }

        public Restaurant Get(int id)
        {
            return _restaurants.FirstOrDefault(r => r.Id == id);
        }

        public void Add(Restaurant newRestaurant)
        {
            newRestaurant.Id = _restaurants.Max(r => r.Id) + 1;
            _restaurants.Add(newRestaurant);
        }

        public int Commit()
        {
            return 0;
        }

        static List<Restaurant> _restaurants;
    }
}
