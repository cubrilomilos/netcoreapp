﻿using NetCoreApp.Entities;
using System.ComponentModel.DataAnnotations;

namespace NetCoreApp.ViewModels
{
    public class RestaurantEditViewModel
    {
        [Required, MaxLength(80)]
        public string Name { get; set; }
        public CUISINE_TYPE Cuisine { get; set; }
    }
}
