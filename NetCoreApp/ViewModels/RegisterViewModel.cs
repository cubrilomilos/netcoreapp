﻿using System.ComponentModel.DataAnnotations;

namespace NetCoreApp.ViewModels
{
    public class RegisterViewModel
    {
        [Required, MaxLength]
        public string Username { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password), Compare(nameof(Password))]
        public string ConfirmPassword { get; set; }
    }
}
