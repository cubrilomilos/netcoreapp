﻿using NetCoreApp.Entities;
using System.Collections.Generic;

namespace NetCoreApp.ViewModels
{
    public class HomePageViewModel
    {
        public IEnumerable<Restaurant> Restaurants { get; set; }
        //public string CurrentGreeting { get; set; }
    }
}
