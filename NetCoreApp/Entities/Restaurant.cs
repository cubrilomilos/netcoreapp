﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreApp.Entities
{
    public enum CUISINE_TYPE
    {
        None,
        Italian,
        French,
        German
    }

    public class Restaurant
    {
        public int Id { get; set; }

        [Required, MaxLength(80)]
        [Display(Name="Restaurant Name")]
        public string Name { get; set; }
        public CUISINE_TYPE Cuisine { get; set; }
    }
}
