﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace NetCoreApp.Entities
{
    public class NetCoreDbContext : IdentityDbContext<User>
    {
        public DbSet<Restaurant> Restaurants { get; set; }

        public NetCoreDbContext(DbContextOptions options) : base(options)
        {

        }
    }
}
